<?php

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

if (!function_exists('sendError')) {
    function sendError($message = '' , $code = 500)
    {
        $response = [
            'data' => null,
            'message' => $message,
            'code' => $code
        ];
        return response()->json($response, $code);
    }
}

if (!function_exists('sendResponse')) {
    function sendResponse($result = null,$message = '' , $code = 200)
    {
        $response = [
            'data' => $result,
            'message' => $message,
            'code' => $code,
        ];
        return response()->json($response, $code);
    }
}


if (!function_exists('settings')) {
    function settings($key=null){
        Cache::flush();
        $settings = Cache::remember('settings', 60*24 , function () {
            return Setting::all([
                'name','value'
            ])
            ->keyBy('name')
            ->transform(function ($setting) {
                 return $setting->value;
            })->toArray(); 
        });
        if ($key == null) {
            return $settings;
        }else {
            return $settings[$key] ?? null; 
        }

    }
}