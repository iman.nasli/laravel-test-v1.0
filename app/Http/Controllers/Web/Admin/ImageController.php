<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Models\Image;
use ImageLib;
// use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::all();
        $readonly = true;
        $edit = false;
        return view('pages.images.index',compact('images','readonly','edit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        $readonly = false;
        return view('pages.images.edit-add',compact('readonly','edit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd();
        $request->validate([
            'title'=>['required','string'],
            'image' =>'required',
        ]);
        $data = $request->except(['_token', 'image']);

        $image       = $request->file('image');
        $filename    = $image->getClientOriginalName();

        $image_resize = ImageLib::make($image->getRealPath());
        $image_resize->resize(900, 600);
        $image_resize->save(public_path('storage/images/').time().'_'.$filename);

        $data['path'] =  'storage/images/' . $image_resize->basename;
        $image = Image::create($data);
        return redirect()->route('images.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $image = Image::find($id);
        $edit = false;
        $readonly = true;
        return view('pages.images.edit-add',compact('readonly','edit','image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = null)
    {
        $image = Image::findOrFail($id);
        $edit = true;
        $readonly = false;
        return view('pages.images.edit-add',compact('image','edit','readonly'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>['required','string'],
        ]);
        $data = $request->except(['_method','_token']);

        if($request->hasFile('image')){
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName();

            $image_resize = ImageLib::make($image->getRealPath());
            $image_resize->resize(900, 600);
            $image_resize->save(public_path('storage/images/').time().'_'.$filename);

            $data['path'] =  'storage/images/' . $image_resize->basename;
        }
        $image = Image::findOrFail($id)->update($data);
        return redirect()->route('images.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Image::destroy($id);
        return redirect()->route('images.index');
    }
}
