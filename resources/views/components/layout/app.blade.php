<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{{env('APP_NAME')}} - {{$title}}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

        {{-- Plugins css --}}
        @stack('plugin-css')

        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

        @stack('css')
    </head>

    <body>

        <div id="wrapper">
            <x-includes.topbar title="{{$title}}"/>
            <x-includes.left-sidebar/>
            <div class="content-page" id="content-page">
                <div class="content">
                    <div class="container-fluid">
                        {{ $slot }}
                    </div>
                </div>
            </div>
            <x-includes.footer/>
        </div>

        <script src="{{asset('assets/js/vendor.min.js')}}"></script>
        @stack('plugin-js')
        <script src="{{asset('assets/js/app.min.js')}}"></script>
    </body>
</html>
