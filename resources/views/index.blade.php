<x-layout.app title="{{__('Logged in')}}">
    <div class="text-center p-5">
        {{__('Welcome')}} <span class="font-weight-bold">{{user()->name}}</span>
    </div>
</x-layout.app>
