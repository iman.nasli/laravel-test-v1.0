<x-layout.data-table title="{{__('Users')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                        <a href="{{route('users.create')}}" class="btn btn-success waves-effect  waves-light">
                            <i class="fas fa-plus"></i>
                            {{__('Add User')}}
                        </a>
                    </div>
                </div>
                <div class=" dt-responsive">
                    <table id="datatable" class="table table-bordered dt-responsive nowrap mt-5">
                        <thead>
                        <tr>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Username')}}</th>
                            <th>{{__('Created At')}}</th>
                            <th data-orderable="false">{{__('Actions')}}</th>
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->username}}</td>
                                <td>{{$user->created_at}}</td>
                                <td>
                                    <a href="{{route('users.show',['user'=>$user->id])}}" class="btn btn-info waves-effect btn-sm waves-light">
                                        <i class="far fa-eye"></i>
                                        {{__('View')}}
                                    </a>
                                    <a href="{{route('users.edit',['user'=>$user->id])}}" class="btn btn-warning waves-effect btn-sm waves-light">
                                        <i class=" fas fa-edit"></i>
                                        {{__('Edit')}}
                                    </a>
                                    @if ($user->id != user()->id)
                                    <a  href="javascript" onclick="$('#modal-form').attr('action','{{route('users.destroy',['user'=>$user->id])}}');
                                        $('#delete-name').text('{{$user->name}}')""  data-toggle="modal" data-target="#modal" class="btn btn-danger  waves-effect btn-sm waves-light">
                                        <i class="fas fa-trash"></i>
                                        {{__('Delete')}}
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('Delete',['attr'=>__('User')])" :body="__('User')"/>

</x-layout.data-table >
