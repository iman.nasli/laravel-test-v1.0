
<x-layout.form :title="__('Add Users')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{($edit?? false) ?route('users.update',['user'=>$user->id]):  route('users.store') }}" data-parsley-validate novalidate autocomplete="off">
                    @if ($edit)
                        @method('put')
                    @endif
                    @if ($readonly)
                        @method('get')
                    @endif
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="name">{{__('Name')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="name" :value="old('name',$user['name']??'')" :placeholder="__('Name')" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="username">{{__('Username')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="username" :value="old('username',$user['username']??'')" :placeholder="__('Username')" />
                        </div>
                    </div>

                    @if (!$edit && !$readonly)
                    <div class="row d-flex justify-content-center">
                        <div class="form-group col-md-6">
                            <label for="username">{{__('Password')}}*</label>
                            <x-UI.forms.input name="password" :placeholder="__('Password')"  />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="username">{{__('Confirm Password')}}*</label>
                            <x-UI.forms.input name="password_confirmation" :placeholder="__('Confirm Password')" />
                        </div>
                    </div>
                    @endif


                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ ($edit ? __('Edit'):__('Add',['attr'=>' '])) }}
                            </button>
                            @endif
                            <a  href="{{route('users.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('Back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
