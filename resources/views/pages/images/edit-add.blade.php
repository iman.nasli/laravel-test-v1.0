@push('plugin-css')
<link href="{{asset('assets/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css" />

@endpush

@push('plugin-js')
<script src="{{asset('assets/libs/dropify/dropify.min.js')}}"></script>
<script>
    $(function(){
        $('.dropify').dropify();
    })
</script>
@endpush

<x-layout.form :title="__('Add Users')">

    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" enctype="multipart/form-data" method="POST" action="{{($edit?? false) ?route('images.update',['image'=>$image->id]):  route('images.store') }}" data-parsley-validate novalidate autocomplete="off">
                    @if ($edit)
                        @method('put')
                    @endif
                    @if ($readonly)
                        @method('get')
                    @endif
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="name">{{__('Title')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="title" :value="old('title',$image['title']??'')" :placeholder="__('Title')" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="username">{{__('Image')}}*</label>
                            <input type="file" {{$readonly ? 'disabled':''}}  class="dropify" name="image" data-default-file="{{isset($image['path'])? asset($image['path']) : ''}}" {{!$edit? 'required': ''}} />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="name">{{__('Target URL')}}</label>
                            <x-UI.forms.input :required=false :readonly=$readonly name="target_url" :value="old('target_url',$image['target_url']??'')" :placeholder="__('Target URL')" />
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ ($edit ? __('Edit'):__('Add',['attr'=>' '])) }}
                            </button>
                            @endif
                            <a  href="{{route('images.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('Back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
