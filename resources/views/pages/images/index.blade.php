<x-layout.data-table title="{{__('Images')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                        <a href="{{route('images.create')}}" class="btn btn-success waves-effect  waves-light">
                            <i class="fas fa-plus"></i>
                            {{__('Add Image')}}
                        </a>
                    </div>
                </div>
                <div class=" dt-responsive">
                    <table id="datatable" class="table table-bordered dt-responsive nowrap mt-5">
                        <thead>
                        <tr>
                            <th>{{__('Title')}}</th>
                            <th>{{__('image')}}</th>
                            <th>{{__('Target URL')}}</th>
                            <th data-orderable="false">{{__('Actions')}}</th>
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($images as $image)
                            <tr>
                                <td>{{$image->title}}</td>
                                <td><img src="{{asset($image->path)}}" alt="" height="100"></td>
                                <td>{{$image->target_url}}</td>
                                <td>
                                    <a href="{{route('images.show',['image'=>$image->id])}}" class="btn btn-info waves-effect btn-sm waves-light">
                                        <i class="far fa-eye"></i>
                                        {{__('View')}}
                                    </a>
                                    <a href="{{route('images.edit',['image'=>$image->id])}}" class="btn btn-warning waves-effect btn-sm waves-light">
                                        <i class=" fas fa-edit"></i>
                                        {{__('Edit')}}
                                    </a>
                                    <a  href="javascript" onclick="$('#modal-form').attr('action','{{route('images.destroy',['image'=>$image->id])}}');
                                        $('#delete-name').text('{{$image->title}}')""  data-toggle="modal" data-target="#modal" class="btn btn-danger  waves-effect btn-sm waves-light">
                                        <i class="fas fa-trash"></i>
                                        {{__('Delete')}}
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('Delete',['attr'=>__('Image')])" :body="__('Image')"/>
</x-layout.data-table >
