$(document).ready(function () {
    $("#datatable").DataTable({
        // language: {
        //     url: '../../assets/libs/datatables/translate.json'
        // },
        iDisplayLength:50
    });
    $(".datatable").DataTable({
        // language: {
        //     url: '../../assets/libs/datatables/translate.json'
        // },
        responsive: true,
        iDisplayLength:50
    }).columns.adjust()
    .responsive.recalc();
    var a = $("#datatable-buttons").DataTable({
        lengthChange: !1,
        buttons: ["copy", "excel", "pdf"],
    });
    $("#key-table").DataTable({ keys: !0 }),
        $("#responsive-datatable").DataTable(),
        $("#selection-datatable").DataTable({ select: { style: "multi" } }),
        a
            .buttons()
            .container()
            .appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)");
});
